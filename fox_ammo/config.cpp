class CfgPatches {
	class fox_ammo {
		units[] = {"fox_launcher","fox_explosive"};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"fox_core"};
	};
};

class CfgVehicles {

	class NATO_Box_Base;

	class fox_crate_base: NATO_Box_Base {
			scope = 0;
			vehicleClass = "fox_crates";
			author = "[FOX] Shiragami";
			icon = "iconCrateSupp";
			model = "\A3\weapons_F\AmmoBoxes\WpnsBox_F";
	};

	class fox_launcher: fox_crate_base {
		scope = 2;
		displayName = "[FOX] Werfer Kiste";
		hiddenSelectionsTextures[] = {"\fox_core\textures\ammobox_signs_ca_fox.paa","A3\Weapons_F\Ammoboxes\data\AmmoBox_CO.paa"};

		class TransportWeapons
		{
			class _xx_mas_launch_Stinger_F {
				weapon = "mas_launch_Stinger_F";
				count = 2;
			};

			class _xx_rhs_weap_smaw_green {
				weapon = "rhs_weap_smaw_green";
				count = 2;
			};
		};

		class TransportMagazines
		{
			class _xx_Titan_AA {
				magazine = "Titan_AA";
				count = 6;
			};
			class _xx_rhs_mag_smaw_HEAA {
				magazine = "rhs_mag_smaw_HEAA";
				count = 2;
			};
			class _xx_rhs_mag_smaw_HEDP {
				magazine = "rhs_mag_smaw_HEDP";
				count = 2;
			};
		};

		class TransportItems
		{
			class _xx_rhsusf_acc_ACOG2_USMC
			{
				name = "rhsusf_acc_ACOG2_USMC";
				count = 2; //Item Count Here
			};

		};

	};

	class fox_explosive: fox_crate_base {
	scope = 2;
	displayName = "[FOX] Explosiv Kiste";
	hiddenSelectionsTextures[] = {"\fox_core\textures\ammobox_signs_ca_fox.paa","A3\Weapons_F\Ammoboxes\data\AmmoBox_CO.paa"};
	class TransportWeapons
	{
		class _xx_MineDetector {
			weapon = "MineDetector";
			count = 2;
		};

	};

	class TransportMagazines
	{
		class _xx_APERSMine_Range_Mag {
			magazine = "APERSMine_Range_Mag";
			count = 6;
		};
		class _xx_SLAMDirectionalMine_Wire_Mag {
			magazine = "SLAMDirectionalMine_Wire_Mag";
			count = 3;
		};
		class _xx_DemoCharge_Remote_Mag {
			magazine = "DemoCharge_Remote_Mag";
			count = 2;
		};
		class _xx_rhsusf_m112x4_mag {
			magazine = "rhsusf_m112x4_mag";
			count = 3;
		};
		class _xx_SatchelCharge_Remote_Mag {
			magazine = "SatchelCharge_Remote_Mag";
			count = 4;
		};
		class _xx_ATMine_Range_Mag {
			magazine = "ATMine_Range_Mag";
			count = 5;
		};
		class _xx_rhsusf_m112_mag {
			magazine = "rhsusf_m112_mag";
			count = 4;
		};
		class _xx_APERSBoundingMine_Range_Mag {
			magazine = "APERSBoundingMine_Range_Mag";
			count = 5;
		};
	};

	class TransportItems
	{
		class _xx_ACE_DefusalKit{
			name = "ACE_DefusalKit";
			count = 2; //Item Count Here
		};
		class _xx_ACE_M26_Clacker{
			name = "ACE_M26_Clacker";
			count = 2; //Item Count Here
		};

	};
	};

};
