![fox_logo.png](https://bitbucket.org/repo/56Bx5K/images/2802504852-fox_logo.png)

## Classnames:


```
#!

Werferkiste: fox_launcher
Sprengstoffkiste: fox_explosive
```
```
#!

Arsenal: fox_arsenal_basic
Improved Arsenal: fox_arsenal_improved
```
```
#!

Sanitätskiste 'klein': fox_medical_small
Sanitätskiste 'groß': fox_medical_large
```
```
#!

M1025 Desert: fox_m1025w_m2
M1025 Wood: fox_m1025d_m2
```