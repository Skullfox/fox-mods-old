class CfgPatches {
	class fox_core {
		units[] = {};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {};
	};
};

class cfgFactionClasses
{
	class fox_faction
	{
		icon = "fox_core\textures\faction.paa";
		displayName = "FOX";
		priority = 1;
		side = 1;
		author = "[FOX] Shiragami";
	};
};
class CfgVehicleClasses
{
	class fox_wheeled
	{
		displayname = "FOX Radfahrzeug";
	};

	class fox_armored
	{
		displayname = "FOX Gepanzert";
	};
	class fox_crates
	{
		displayname = "FOX Kisten";
	};
};
