class CfgPatches {
	class fox_arsenal {
		units[] = {"fox_arsenal_basic","fox_arsenal_improved"};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Characters_F_BLUFOR","fox_core","fox_ammo"};
	};
};

class CfgVehicles {

	class fox_crate_base;

	class fox_arsenal_base: fox_crate_base{
		showWeaponCargo = false;
	};


	class fox_arsenal_basic: fox_arsenal_base {
		scope = 2;
		displayName = "[FOX] Arsenal Kiste";
		hiddenSelectionsTextures[] = {"\fox_core\textures\ammobox_signs_ca_fox.paa","A3\Weapons_F\Ammoboxes\data\AmmoBox_CO.paa"};

		class TransportWeapons{};
		class TransportMagazines{};

		class UserActions{
			class TakeSensor
			{
				displayNameDefault = "Arsenal";
				priority = 3;
				showWindw = 1;
				hideOnUse = 1;
				displayName = "Arsenal";
				radius= 5;
				position = "camera";
				onlyForPlayer = 1;
				condition = "(alive this)";
				statement = "[this] execVM ""\fox_arsenal\scripts\fox_crate_arsenal.sqf""";
			};
		};
	};


	class fox_arsenal_improved: fox_arsenal_base {
	scope = 2;
	displayName = "[FOX] Improved Arsenal Kiste";
	hiddenSelectionsTextures[] = {"\fox_core\textures\ammobox_signs_ca_fox.paa","A3\Weapons_F\Ammoboxes\data\AmmoBox_CO.paa"};


		class UserActions{
		class TakeSensor{
			displayNameDefault = "Improved Arsenal";
			priority = 3;
			showWindw = 1;
			hideOnUse = 1;
			displayName = "Improved Arsenal";
			radius= 5;
			position = "camera";
			onlyForPlayer = 1;
			condition = "(alive this)";
			statement = "[this] execVM ""\fox_arsenal\scripts\fox_crate_improvedArsenal.sqf""";
			};
		};
	};

};
