class CfgPatches {
	class fox_medical {
		units[] = {"fox_medical_small","fox_medical_large"};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Characters_F_BLUFOR","fox_core"};
	};
};

class CfgVehicles {
	
	class NATO_Box_Base;
	
	class fox_medical_small: NATO_Box_Base {
	scope = 2;
	vehicleClass = "Ammo";
	displayName = "[FOX] San Kiste Klein"; 
	model = "\A3\weapons_F\AmmoBoxes\WpnsBox_F";
	author = "[FOX] Shiragami";
	hiddenSelectionsTextures[] = {"\fox_core\textures\ammobox_signs_ca_fox.paa","A3\Weapons_F\Ammoboxes\data\AmmoBox_CO.paa"};
	icon = "iconCrateSupp";
	class TransportWeapons{};
	class TransportMagazines{};
	class TransportItems
	{
		class _xx_ACE_fieldDressing
		{
			name = "ACE_fieldDressing";
			count = 50; //Item Count Here
		};
		
		class _xx_ACE_elasticBandage
		{
			name = "ACE_elasticBandage";
			count = 25; //Item Count Here
		};
		
		class _xx_ACE_morphine
		{
			name = "ACE_morphine";
			count = 30; //Item Count Here
		};
		class _xx_ACE_epinephrine
		{
			name = "ACE_epinephrine";
			count = 30; //Item Count Here
		};
		
		class _xx_ACE_bloodIV_500
		{
			name = "ACE_bloodIV_500";
			count = 5; //Item Count Here
		};
		
		class _xx_ACE_bloodIV_250
		{
			name = "ACE_bloodIV_250";
			count = 10; //Item Count Here
		};

		class _xx_ACE_surgicalKit
		{
			name = "ACE_surgicalKit";
			count = 3; //Item Count Here
		};

		class _xx_ACE_tourniquet
		{
			name = "ACE_tourniquet";
			count = 4; //Item Count Here
		};

		class _xx_ACE_EarPlugs
		{
			name = "ACE_EarPlugs";
			count = 2; //Item Count Here
		};
		class _xx_ACE_HandFlare_Red
		{
			name = "ACE_HandFlare_Red";
			count = 2; //Item Count Here
		};			
		
	};
	};
	
	class fox_medical_large: NATO_Box_Base {
	scope = 2;
	vehicleClass = "Ammo";
	displayName = "[FOX] San Kiste Groß"; 
	model = "\A3\weapons_F\AmmoBoxes\WpnsBox_F";
	author = "[FOX] Shiragami";
	hiddenSelectionsTextures[] = {"\fox_core\textures\ammobox_signs_ca_fox.paa","A3\Weapons_F\Ammoboxes\data\AmmoBox_CO.paa"};
	icon = "iconCrateSupp";
	class TransportWeapons{};
	class TransportMagazines{};
	class TransportItems
	{
		class _xx_ACE_fieldDressing
		{
			name = "ACE_fieldDressing";
			count = 50; //Item Count Here
		};
		
		class _xx_ACE_elasticBandage
		{
			name = "ACE_elasticBandage";
			count = 25; //Item Count Here
		};
		
		class _xx_ACE_morphine
		{
			name = "ACE_morphine";
			count = 30; //Item Count Here
		};
		class _xx_ACE_epinephrine
		{
			name = "ACE_epinephrine";
			count = 30; //Item Count Here
		};
		
		class _xx_ACE_bloodIV_500
		{
			name = "ACE_bloodIV_500";
			count = 5; //Item Count Here
		};
		
		class _xx_ACE_bloodIV_250
		{
			name = "ACE_bloodIV_250";
			count = 10; //Item Count Here
		};

		class _xx_ACE_surgicalKit
		{
			name = "ACE_surgicalKit";
			count = 3; //Item Count Here
		};

		class _xx_ACE_tourniquet
		{
			name = "ACE_tourniquet";
			count = 4; //Item Count Here
		};

		class _xx_ACE_EarPlugs
		{
			name = "ACE_EarPlugs";
			count = 2; //Item Count Here
		};
		class _xx_ACE_HandFlare_Red
		{
			name = "ACE_HandFlare_Red";
			count = 2; //Item Count Here
		};			
		
	};
	};
			
};