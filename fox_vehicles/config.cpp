class CfgPatches {
	class fox_vehicles {
		units[] = {"fox_m1025w_m2","fox_m1025d_m2","fox_m1025d_m2_cargo_m2"};
		weapons[] = {};
		requiredVersion = 0.1;
		requiredAddons[] = {"fox_core","rhsusf_hmmwv"};
	};
};

class CfgVehicles
{
	class rhsusf_m1025_w_m2;

	class fox_m1025_base : rhsusf_m1025_w_m2 {
		scope = 0;

		vehicleclass = "fox_wheeled"; //Unit Group
		faction = "fox_faction";
		author="RHS | FOX ";

		class TransportWeapons
		{
			class _xx_CUP_glaunch_M32 {
				weapon = "CUP_glaunch_M32";
				count = 1;
			};

			class _xx_srifle_mas_m107_d {
				weapon = "srifle_mas_m107_d";
				count = 1;
			};
			class _xx_arifle_mas_m1014 {
				weapon = "arifle_mas_m1014";
				count = 1;
			};
			class _xx_CUP_lmg_m249_SQuantoon {
				weapon = "CUP_lmg_m249_SQuantoon";
				count = 1;
			};
			class _xx_rhs_weap_m4_grip {
				weapon = "rhs_weap_m4_grip";
				count = 3;
			};
		};

		class TransportMagazines
		{
			class _xx_CUP_6Rnd_HE_M203 {
				magazine = "CUP_6Rnd_HE_M203";
				count = 3;
			};
			class _xx_CUP_6Rnd_Smoke_M203 {
				magazine = "CUP_6Rnd_Smoke_M203";
				count = 3;
			};
			class _xx_5Rnd_mas_127x99_T_Stanag {
				magazine = "5Rnd_mas_127x99_T_Stanag";
				count = 7;
			};
			class _xx_7Rnd_mas_12Gauge_Slug {
				magazine = "7Rnd_mas_12Gauge_Slug";
				count = 10;
			};
			class _xx_CUP_200Rnd_TE4_Red_Tracer_556x45_M249 {
				magazine = "CUP_200Rnd_TE4_Red_Tracer_556x45_M249";
				count = 6;
			};
			class _xx_30Rnd_556x45_Stanag {
				magazine = "30Rnd_556x45_Stanag";
				count = 31;
			};
			class _xx_CUP_6Rnd_FlareGreen_M203 {
				magazine = "CUP_6Rnd_FlareGreen_M203";
				count = 4;
			};
		};

		class TransportItems
		{
			class _xx_ToolKit
			{
				name = "ToolKit";
				count = 1 ; //Item Count Here
			};

		};

	};


	class fox_m1025w_m2: fox_m1025_base{
		scope = 2;
		displayname = "M1025A2 Wood (M2)";
		crew = "B_G_Soldier_F";
		typicalCargo[] = {"B_G_Soldier_F"};
		HiddenSelectionsTextures[] =
		{
			"fox_vehicles\textures\m998_exterior_w_co.paa",
			"rhsusf\addons\rhsusf_hmmwv\textures\m998_interior_w_co.paa",
			"rhsusf\addons\rhsusf_hmmwv\textures\A2_parts_WD_co.paa",
			"rhsusf\addons\rhsusf_hmmwv\textures\wheel_wranglermt_b_co.paa",
			"rhsusf\addons\rhsusf_hmmwv\textures\m998_mainbody_co.paa",
			"rhsusf\addons\rhsusf_hmmwv\textures\gratting_w_co.paa",
			"fox_vehicles\textures\m1025_w_co.paa",
			"rhsusf\addons\rhsusf_hmmwv\textures\mk64mount_w_co.paa",
			"rhsusf\addons\rhsusf_hmmwv\unitdecals\101stab_502reg_2ndbn_a12_w_co.paa"
		};

	};


	class fox_m1025d_m2: fox_m1025_base{
		scope = 2;
		displayname = "M1025A2 Desert (M2)";
		crew = "B_Soldier_F";
		typicalCargo[] = {"B_Soldier_F"};
		HiddenSelectionsTextures[] = {
																	"fox_vehicles\textures\m998_exterior_d_co.paa",
																	"rhsusf\addons\rhsusf_hmmwv\textures\m998_interior_d_co.paa",
																	"rhsusf\addons\rhsusf_hmmwv\textures\A2_parts_D_co.paa",
																	"rhsusf\addons\rhsusf_hmmwv\textures\wheel_wranglermt_d_co.paa",
																	"rhsusf\addons\rhsusf_hmmwv\textures\m998_mainbody_d_co.paa",
																	"rhsusf\addons\rhsusf_hmmwv\textures\gratting_d_co.paa",
																	"fox_vehicles\textures\m1025_d_co.paa",
																	"rhsusf\addons\rhsusf_hmmwv\textures\mk64mount_d_co.paa",
																	"rhsusf\addons\rhsusf_hmmwv\unitdecals\101stab_502reg_2ndbn_a12_d_co.paa",
																	"rhsusf\addons\rhsusf_hmmwv\unitdecals\a12^_co.paa"
																};

	};

	class fox_m1025d_m2_cargo_m2: fox_m1025d_m2{
		displayname = "M1025A2 Desert (M2)Cargo(M2)";


		class Eventhandlers  {
					init = "(_this select 0) execvm ""\fox_vehicles\scripts\m2_cargo_gunner.sqf""";
			};

			HiddenSelectionsTextures[] = {
																		"fox_vehicles\textures\m998_exterior_d_co.paa",
																		"rhsusf\addons\rhsusf_hmmwv\textures\m998_interior_d_co.paa",
																		"rhsusf\addons\rhsusf_hmmwv\textures\A2_parts_D_co.paa",
																		"rhsusf\addons\rhsusf_hmmwv\textures\wheel_wranglermt_d_co.paa",
																		"rhsusf\addons\rhsusf_hmmwv\textures\m998_mainbody_d_co.paa",
																		"rhsusf\addons\rhsusf_hmmwv\textures\gratting_d_co.paa",
																		"fox_vehicles\textures\m1025_d_co_m2cargo.paa",
																		"rhsusf\addons\rhsusf_hmmwv\textures\mk64mount_d_co.paa",
																		"rhsusf\addons\rhsusf_hmmwv\unitdecals\101stab_502reg_2ndbn_a12_d_co.paa",
																		"rhsusf\addons\rhsusf_hmmwv\unitdecals\a12^_co.paa"
																	};

	};



};
