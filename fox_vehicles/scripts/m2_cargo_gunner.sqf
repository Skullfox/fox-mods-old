private ["_vehicle","_gun"];

_vehicle = _this;


[ [ [_vehicle],"\fox_vehicles\scripts\create_m2.sqf"],"BIS_fnc_execVM",false] call BIS_fnc_MP;

_vehicle addAction ["To Cargo (M2)",{

  _g = (_this select 0) getVariable "fox_humvee_m2";
  _p = _this select 1;
  cutText ["","BLACK OUT",0.5];
  _p action [ "eject", _this select 0];
  sleep 1.5;
  cutText ["","BLACK IN",0.5];
  _p moveInGunner _g;

},"", 0, false, false, "", "_this in _target"];



  _gun addAction ["To Humvee", {

  _h = _this select 0;
  _v = (_this select 0) getVariable "fox_humvee";
  _p = _this select 1;

  cutText ["","BLACK OUT",0.5];
  sleep 0.5;
  _p allowDamage false;
  _p setpos [0,0,0];
  sleep 0.5
  _pl switchmove "";
  sleep 0.5
  cutText ["","BLACK IN",0.5];
  _p moveInCargo _v;
  _p allowDamage true;
  
 } ,"", 0, false, false, "", "_this in _target"];

/*
TOW
t attachTo [h, [-0.4, -0.86, 1.55] ];t setdir 180; player moveInGunner t
Mörser
t attachTo [h, [-0.5, -2.15, 1.55] ];t setdir 0;

*/
