
_vehicle = _this select 0;

_gun = "CUP_B_M2StaticMG_MiniTripod_US" createVehicle position _vehicle;
_gun setdir (getdir _vehicle - 180);
_gun attachTo [_vehicle, [-0.45, -1.1, 1.7] ];

_vehicle setVariable ["fox_humvee_m2", _gun,true];
_gun setVariable ["fox_humvee", _vehicle,true];